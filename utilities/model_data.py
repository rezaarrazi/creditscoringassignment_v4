class ModelData(dict):
	def __getattr__(self, name):
		return self[name]

	def __getstate__(self):
		return self.__dict__

	def __setstate__(self, state):
		self.__dict__ = state