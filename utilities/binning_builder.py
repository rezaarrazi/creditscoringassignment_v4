import pandas as pd
import numpy as np
import json
import pickle
import warnings
import matplotlib.pyplot as plt
import plotly.express as px
import datetime
import time
import seaborn as sns
from optbinning import OptimalBinning
import warnings

from utilities.model_data import ModelData

class BinningBuilder(object):
	DEFAULT_WOE = 0 

	def __init__(self, train, key='id', flag='flag'):
		self.raw_train = train.copy()
		self.key = key
		self.flag = flag

		self.train = ModelData()
		self.train['data'] = train.copy()
		self.train['data_woe'] = self.train['data'][[self.key, self.flag]].copy()
		self.train['data_bin'] = self.train['data'][[self.key, self.flag]].copy()

		self.meta_data = {} 
		
		if (self.train['data'][self.flag].nunique() != 2):
			raise ValueError("The target column must be binary")
		
		self.error_handler = {}
		
	@staticmethod
	def create_binning_from_list(keys, value, existing_binning={}):
		"""
		Reverses one-to-many mappings to be inputted as binning data.

		Parameter
		------
		keys: list, List of strings to be used as keys to the binning dictionary
		value: str, Value to every key in the dictionary
		existing_binning: dict, Existing dictionary to be added key,value pairs if applicable
		"""
		binning_data = existing_binning
		for key in keys:
			binning_data[str(key)] = str(value)
		return binning_data

	@staticmethod
	def get_default_categorical_binning(series):
		"""
		Create default categorical. Example, [a,b,c,b,c] will product the default binning of {'a': 'a', 'b':'b', 'c':'c'}

		Parameter
		------
		series: pd.Series, data in which the unique values will be converted to the binning dictionary
		"""
		binning_data = {}
		for val in series.unique():
			binning_data[str(val)] = str(val)
		return binning_data

	@staticmethod
	def calculate_iv(pivot_table, iv='IV'):
		"""
		Calculates information value given a pivot table with iv value in the column 'IV'

		Parameter
		------
		pivot_table: pd.DataFrame, table with the information value column
		iv: str, If other column names are used for iv
		"""
		iv = pivot_table[(pivot_table.IV.notnull()) & (pivot_table.IV != np.inf) & (pivot_table.IV  != -np.inf)]['IV'].sum()
		return iv

	@staticmethod
	def display_graph(pivot_table):
		"""
		Displays WOE values for each binning in the pivot table, for the purpose of faster recognition of binning appropriateness.

		Parameter
		------
		pivot_table: pd.DataFrame, table with the information value column
		"""
		x_labels = pivot_table.index
		x_range = list(range(len(x_labels)))
		ax = plt
		ax.bar(x_range, pivot_table['WOE'])
		plt.xticks(x_range, x_labels, rotation=90)
		plt.xlabel('bins')

	def reset(self, cols=None):
		"""
		Resets data to raw_data, this means: New columns added and null handlers will be back to normal

		Parameter
		------
		pivot_table: pd.DataFrame, table with the information value column
		cols: list, If defined, the columns that are reverted to its raw data will be limited to the values in the list
		"""
		if cols is None:
			self.train['data'] = self.raw_train.copy()
		else:
			for col in cols:
				self.train['data'] = self.raw_train[col]

		self.train['data_bin'] = self.train['data'][[self.key, self.flag]].copy()

	def add_column(self, col_name, data):
		"""
		Adds a new column to the data set.

		Parameter
		------
		col_name: Str, name of column 
		data: pd.Series, Series for the column
		"""
		self.train['data'][col_name] = data

	@staticmethod
	def load_meta_data_from_file(path):
		"""
		Gets meta data from a file. This means, information on binning and null handling will be interpreted from the
		supplied meta data during transformation.

		Parameter
		------
		data: pd.Series, Series for the column
		cols: columns to be merged into the current data
		key: key from the new data frame to be mapped into current data
		"""
		meta_data = pd.read_csv(path, index_col=0).to_dict()
		for key in meta_data:
			meta_data[key]['binning_data'] = json.loads(meta_data[key]['binning_data'])
			if meta_data[key]['data_type'] == 'numerical':
				meta_data[key]['default_null'] = float(meta_data[key]['default_null'])
				
		return meta_data

	def get_meta_data_from_file(self, path):
		"""
		Gets meta data from a file. This means, information on binning and null handling will be interpreted from the
		supplied meta data during transformation.

		Parameter
		------
		data: pd.Series, Series for the column
		cols: columns to be merged into the current data
		key: key from the new data frame to be mapped into current data
		"""
		meta_data = BinningBuilder.load_meta_data_from_file(path)
		for key in meta_data:
			self.meta_data[key] = meta_data[key]
		
	def save_meta_data_to_file(self, path):
		to_save = self.meta_data.copy()
		for key in to_save:
			binning_data = to_save[key]['binning_data']
			to_save[key]['binning_data'] = json.dumps(binning_data)
		meta_data = pd.DataFrame(to_save)
		meta_data.to_csv(path)
		
	def get_meta_data_from_dict(self, meta_data):
		for key in meta_data:
			self.meta_data[key] = meta_data[key]
		
	def save_meta_data_to_dict(self):
		return self.meta_data

	def get_binning(raw_values, meta_data_):
		# handle null 
		raw_values = raw_values.fillna(meta_data_['default_null'])

		# binning mapping
		binning_data = meta_data_['binning_data']
		categorical = meta_data_['data_type'] == 'categorical'
		if categorical:
			bin_values = raw_values.astype(str).apply(lambda x: binning_data[x] if x in binning_data else np.nan)
		else:
			bin_values = pd.cut(raw_values, binning_data) 
		if categorical:
			# handle default category
			bin_values = bin_values.fillna(meta_data_['default_bin'])
		else:
			# handle default bin
			default_bin = meta_data_['default_bin']
			bin_values = bin_values.cat.add_categories(default_bin).fillna(default_bin)
		return bin_values
		
	def calculate_ivs(self, cols):
		ivs = pd.DataFrame({'features':[], 'iv':[]})
		for col in cols:
			if col in self.train['data_bin'].columns.tolist():
				pivot = BinningBuilder.calculate_woe(self.train['data_bin'], col, self.flag, key=self.key)
				
				iv = BinningBuilder.calculate_iv(pivot)
				ivs = ivs.append(pd.Series({'features':col, 'iv':iv}), ignore_index=True)
			else:
				warnings.warn("cannot find {col} in metadata, run create_binning for this column first".format(col))
				
		return ivs.sort_values(by='iv', ascending=False)
		
	def create_binning(self, col, binning_data=None, auto_binning=False, print_binning_data=False,
			 categorical=False, solver='cp', default_bin=None, default_null=None):
		"""
			

		Parameter
		------
		col: str, the data column for WOE transform with optimal binning
		categorical: Bool,  When True, coerces feature to be treated as categorical. Else, the type will be inferred from data type.
		auto_binning: Bool, When True uses optimal_binning. 
		solver: str, the optimizer to solve the optimal binning problem
				Supported solvers are:
				(1) “mip” to choose a mixed-integer programming solver,
				(2) “cp” to choose a constrained programming solver
		
		"""
		# coerce categorical when categorical = True
		data_type = self.train['data'][col].dtype 
		if categorical == False:
			if data_type != 'int64' and data_type != 'float64': 
				categorical = True

		if binning_data is None and col not in self.meta_data and auto_binning == False:
			raise ValueError("Binning data has to be defined for categorical AND numerical data. List for numerical, dictionary for categorical.")

		if binning_data is None and auto_binning == False:
			raise ValueError("Binning data has to be defined if auto_binning set to False.")	
		
		# determine binning
		if auto_binning:
			binning_data = BinningBuilder.optimal_binning(self.train.data[col], self.train.data[self.flag].values, solver=solver)
		
		if print_binning_data:
			print("binning_data:", binning_data)

		if col not in self.meta_data:
			self.meta_data[col] = {}

		if default_null is None and 'default_null' not in self.meta_data[col]:
			self.meta_data[col]['default_null'] = -9999999999 
		elif default_null is not None:
			self.meta_data[col]['default_null'] = default_null

		if default_bin is None and 'default_bin' not in self.meta_data[col]:
			self.meta_data[col]['default_bin'] = "UNBINNED"
		elif default_bin is not None:
			self.meta_data[col]['default_bin'] = default_bin

		self.meta_data[col]['data_type'] = "categorical" if categorical else "numerical" 
		if binning_data is not None:
			self.meta_data[col]['binning_data'] = binning_data

		# save state
		self.train['data_bin'][col] = BinningBuilder.get_binning(self.train['data'][col], self.meta_data[col])
	
	def create_binning_and_calculate_woe(self, col, binning_data=None, auto_binning=False, print_binning_data=False,
			 	categorical=False, solver='cp', default_bin=None, default_null=None,
				print_iv=False, filter_column=None, display_graph=True):
		"""
			

		Parameter
		------
		
		"""
		self.create_binning(col, binning_data=binning_data, print_binning_data=print_binning_data, 
			 categorical=categorical, solver=solver, auto_binning=auto_binning, default_bin=default_bin, default_null=default_null)
		
		data = self.train['data_bin']
		if filter_column is not None:
			data = self.train['data_bin'][self.train['data'][filter_column]]
		
		pivot_table = BinningBuilder.calculate_woe(data, col, self.flag, key=self.key)

		if print_iv:
			print("IV: %2f" % BinningBuilder.calculate_iv(pivot_table))

		if display_graph:
			BinningBuilder.display_graph(pivot_table)

		return pivot_table
	
	@staticmethod
	def optimal_binning(series, flag, solver='cp'):
		"""
		Determine the optimal binning

		Parameter
		------
		series: pandas series, the data column for determining the optimal binning
		flag: numpy array, the target data
		solver: str, the optimizer to solve the optimal binning problem
				Supported solvers are:
				(1) “mip” to choose a mixed-integer programming solver,
				(2) “cp” to choose a constrained programming solver
		"""

		# turn a list to a string
		def list_to_string(str_in):
			str_out = ""
			for ele in str_in:
				str_out += ele
				str_out += ' + '
			return str_out[:-3]

		col = series.name
		# determine binning
		dtype = type(series.values[0])
		if dtype is np.float64 or dtype is np.int64:
			# For numerical data
			optb = OptimalBinning(name=col, dtype="numerical", solver=solver)
			optb.fit(series.values, flag)
			output = optb.splits.tolist()
			output.append(np.inf)
			output.insert(0, -np.inf)
		else:
			# For categorical data
			optb = OptimalBinning(name=col, dtype="categorical", solver=solver)
			optb.fit(series.values, flag)
			output = {}
			for i in range(len(optb.splits)):
				bin_list = optb.splits[i].tolist()
				target_bin = list_to_string(bin_list)
				for ele in bin_list:
					output[ele] = target_bin
		return output

	@staticmethod
	def calculate_woe(data, index_column, target_column, key='orderId'):
		pivot_table = pd.pivot_table(data, index=[index_column], columns=[target_column], values=key, aggfunc='count')
		
		pivot_table = pivot_table.fillna(0)
		pivot_table["TOTAL"] = pivot_table[1]+pivot_table[0]
		pivot_table['BADRATE'] = pivot_table[1]/pivot_table["TOTAL"]
		pivot_table['DIST_BAD'] = pivot_table[1]/pivot_table[1].sum()
		pivot_table['DIST_GOOD'] = pivot_table[0]/pivot_table[0].sum()
		pivot_table['WOE'] = np.log(pivot_table['DIST_BAD']/pivot_table['DIST_GOOD'])
		pivot_table['IV'] = pivot_table['WOE'] * (pivot_table['DIST_BAD'] - pivot_table['DIST_GOOD'])
		return pivot_table

	def multi_pivot(self, cols):
		pivot_table = pd.pivot_table(self.train['data_bin'], index=cols, columns=[self.flag], values=self.key, aggfunc='count')
		pivot_table = pivot_table.fillna(0)
		pivot_table["TOTAL"] = pivot_table[1]+pivot_table[0]
		pivot_table['BADRATE'] = pivot_table[1]/pivot_table["TOTAL"]
		pivot_table['DIST_BAD'] = pivot_table[1]/pivot_table[1].sum()
		pivot_table['DIST_GOOD'] = pivot_table[0]/pivot_table[0].sum()
		pivot_table['WOE'] = np.log(pivot_table['DIST_BAD']/pivot_table['DIST_GOOD'])
		pivot_table['IV'] = pivot_table['WOE'] * (pivot_table['DIST_BAD'] - pivot_table['DIST_GOOD'])
		return pivot_table 

	@staticmethod
	def bin_to_woe(data_bin, mapper, default_bin, verbose=False):
		col = data_bin.name
		
		if default_bin not in mapper:
			mapper[default_bin] = BinningBuilder.DEFAULT_WOE
		for binning in data_bin.unique():
			if binning not in mapper:
				mapper[binning] = BinningBuilder.DEFAULT_WOE
		data_woe = data_bin.replace(mapper)
		if verbose and data_woe.isna().any():
			print("Warning: Default WOE values used for col %s because some binnings are not available in mapping"%col)
		data_woe = data_woe.replace(np.nan, BinningBuilder.DEFAULT_WOE)
		if verbose and ((data_woe == np.inf) | (data_woe == -np.inf)).any():
			print("Warning: Default WOE values used for col %s because some binnings values are inf"%col)
		data_woe = data_woe.replace(np.inf, BinningBuilder.DEFAULT_WOE)
		data_woe = data_woe.replace(-np.inf, BinningBuilder.DEFAULT_WOE)
		return data_woe

	@staticmethod
	def woe_transform(data_bin, variables, meta_data, mappers, key='id', flag='flag'):
		data_woe = data_bin[[key, flag]].copy()

		for col in variables:
			if col not in meta_data:
				raise ValueError("Meta data information on binning & default values are not defined for %s"%col)
			
			data_woe[col] = BinningBuilder.bin_to_woe(data_bin[col], mappers[col], meta_data[col]['default_bin'])

		return data_woe
	
	@staticmethod
	def data_to_binning(data, variables, meta_data, key='id', flag='flag'):
		data_bin = data[[key, flag]].copy()

		for col in variables:
			if col not in meta_data:
				raise ValueError("Meta data information on binning & default values are not defined for %s"%col)
			
			data_bin[col] = BinningBuilder.get_binning(data[col], meta_data[col])

		return data_bin
	
	@staticmethod
	def generate_woe_mappers(data_bin, variables, meta_data, key='id', flag='flag'):
		mappers = {}
	
		for col in variables:
			if col not in meta_data:
				raise ValueError("Meta data information on binning & default values are not defined for %s"%col)
			
			mappers[col] = BinningBuilder.get_woe_mapper(data_bin, col, flag, key=key)

		return mappers

	@staticmethod
	def get_woe_mapper(data, index_column, target_column, key='orderId'):
		pivot_table = BinningBuilder.calculate_woe(data, index_column, target_column, key)
		return pivot_table[['WOE']].to_dict()['WOE']

	def generate_kd_plot(self, col, data=None):
		"""
		Displays kernel density plot to visualize difference in distribution between 0 and 1 

		Parameter
		------
		pivot_table: pd.DataFrame, table with the information value column
		"""
		if data is None:
			data = self.train['data']

		bad_data = data[data[self.flag] == 1]
		good_data = data[data[self.flag] == 0]
		
		sns.kdeplot(data=bad_data[col], label="bad", color="red")
		sns.kdeplot(data=good_data[col], label="good", color="blue")

