import pandas as pd
import numpy as np

from sklearn.model_selection import cross_val_score, StratifiedShuffleSplit
from sklearn.metrics import roc_auc_score, precision_score, recall_score, f1_score, auc, roc_curve
from sklearn.base import clone
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.feature_selection import RFE
from sklearn.inspection import permutation_importance
from collections import OrderedDict
from sklearn.metrics import confusion_matrix, plot_roc_curve
import optuna

import catboost as cgb
import xgboost as xgb
from xgboost import XGBClassifier
from catboost import Pool, CatBoostClassifier
from scipy import interp
import warnings
import pickle
import os
import IPython
import json
import matplotlib.pyplot as plt
import enum
import pickle
from utilities.binning_builder import BinningBuilder
from utilities.model_data import ModelData

class Model(object):
	"""
	Model(data, cv=None, key='id', flag='flag')

	A class used for creating score cards mainly used for Indodana's credit scoring.

	Parameter
	---------
	data : pandas.DataFrame
		DataFrame that contains main data used for training the model
	cv : Cross Validation model for training
		Default: StratifiedShuffleSplit(n_splits=5,random_state=0)
	key : str
		Unique identifier for the data
	flag: str
		Column for the target variable
	"""
	
	"""
	Constant
	---------
	PDO: float
		 Point to double the odds value for score scaling process
		 Default: 20
	ODDS: float
		 Odds for score scaling process, usually 50 good contracts per 1 bad contracts at desired score (S0)
		 Default: 50
	S0: float
		 Score for certain odds used in score scaling process, 500 means that at that score, the odds will be 50:1
		 Default: 500
	"""

	PDO = 20
	ODDS = 50
	S0 = 500
	
	def __init__(self, train, test, cv=None, key='id', flag='flag'):
		if (not np.issubdtype(train[flag].dtype, np.number)) or (not np.issubdtype(test[flag].dtype, np.number)):
			raise TypeError("Target data must be numeric")
		self.default_metrics = ['roc_auc', 'precision', 'recall', 'f1', 'conf_matrix', 'score_metrics']
		self.model = ModelData()
		if cv is None:
			cv = StratifiedShuffleSplit(n_splits=5,random_state=0)
		self.cv = cv

		self.train = ModelData()
		self.test = ModelData()

		self.train['data'] = train
		self.test['data'] = test

		self.variables = None
		self.default_woe = 0 
		self.meta_data = {} 
		self.pivot_tables = {}
		self.key = key
		self.flag = flag

		self._calculate_constants()
		self.base_point = None

	def __getstate__(self):
		return self.__dict__

	def __setstate__(self, state):
		self.__dict__ = state

	def load_model_from_pickle(self, model_index, filename):
		pickle_in = open(filename,"rb")
		self.model[model_index] = pickle.load(pickle_in)

	def save_model_to_pickle(self, model_index, filename):
		pickle_out = open(filename,"wb")
		pickle.dump(self.model[model_index], pickle_out)

	def load_from_pickle(self, filename):
		pickle_in = open(filename,"rb")
		self = pickle.load(pickle_in)
	
	def save_to_pickle(self, filename):
		pickle_out = open(filename,"wb")
		pickle.dump(self, pickle_out)

	def load_meta_data_from_dict(self, meta_data):
		self.meta_data = meta_data

	def load_meta_data_from_file(self, path):
		meta_data = BinningBuilder.load_meta_data_from_file(path)
		self.meta_data = meta_data

	def _calculate_constants(self):
		self.M = Model.PDO / np.log(2.)
		self.C = Model.S0 + (self.M * np.log(1./Model.ODDS))

	def get_numerical_data_frame(self, variables, data=None):
		"""
		Convert data frame for easy training on other models
		"""
		if data is None:
			self.woe_transform(variables, auto_binning=True)
			train = self.train['data'][[self.key, self.flag] + variables].copy()
			test = self.test['data'][[self.key, self.flag] + variables].copy()

			for col in variables:
				if self.meta_data[col]["data_type"] != "numerical":
					train[col] = self.train['data_woe'][col]
					test[col] = self.test['data_woe'][col]
				else:
					default_null = self.meta_data[col]["default_null"]
					train[col] = train[col].fillna(default_null)
					test[col] = test[col].fillna(default_null)

			return train, test
		else:
			data = data.copy()
			data_bin = pd.DataFrame()
			for col in self.variables:
				default_null = self.meta_data[col]["default_null"]
				if self.meta_data[col]["data_type"] != "numerical":
					data_bin[col] = BinningBuilder.get_binning(data[col], self.meta_data[col])
					mapper = self.train['mapper'][col]
					data[col] = self.bin_to_woe(data_bin[col], mapper, col).fillna(default_null)
				else:
					data[col] = data[col].fillna(default_null)
			return data

		
	def transform(self, variables, auto_binning=False, default_woe=0):
		self.variables = variables

		self.train['data_bin'] = BinningBuilder.data_to_binning(self.train['data'], variables, self.meta_data)
		self.train['mapper'] = BinningBuilder.generate_woe_mappers(self.train['data_bin'], variables, self.meta_data)
		self.train['data_woe'] = BinningBuilder.woe_transform(self.train['data_bin'], variables, self.meta_data, self.train['mapper'])

		self.test['data_bin'] = BinningBuilder.data_to_binning(self.test['data'], variables, self.meta_data)
		self.test['data_woe'] = BinningBuilder.woe_transform(self.test['data_bin'], variables, self.meta_data, self.train['mapper'])

	def get_prediction(self, series, threshold_value, threshold_type):
		"""
		Get prediction (binary) given a probability or score dataset and threshold value.

		Parameter
		---------
		- series: pandas.Series, score or probability series that will be transformed into binary class
		- threshold_value: float, score or probability threshold
		- threshold_type: str, options: score, probability.

		Return
		---------
		pandas.Series, predicted class data
		"""
		self._check_threshold_type(threshold_type)

		if threshold_type == 'score':
			return series.apply(lambda x: self._score_threshold(x, threshold_value))
		elif threshold_type == 'probability':
			return series.apply(lambda x: self._proba_threshold(x, threshold_value))

	def score_distribution(self, score, target, interval=10., min_score=300., max_score=480., threshold_value=330., name='model'):
		"""
		Create score distribution for model evaluation

		Parameter
		---------
		- score: pandas.Series, data series to be grouped
		- target: pandas.Series, target data series to create bad rate for every score bands
		- interval: float, bins interval, default: 20.

		Optional Parameter
		- min_score: float, upper boundary of lowest class (lowest class will be -np.inf to min_score)
					default: 300.
		- max_score: float, lower boundary of highest class (will be max_score to np.inf)
					default: 480.
		- threshold_value: float, score threshold to calculate population above and below threshold
					default: 330.

		Return
		------
		- score_table: pandas.DataFrame, score distribution pivot table containing
					   BADRATE, DIST_BAD, DIST_GOOD, DIST
		- distribution_metrics: dict, dictionary containing score metrics containing
					 name: dictionary name
					 badrate: bad rate in total population
					 population_above_threshold: % population above score threshold
					 population_below_threshold: % population below score threshold
					 cumulative_bad_rate_above_threshold: cumulative bad rate above score threshold
					 threshold: threshold value

		"""
		bins = np.arange(min_score, max_score, interval)
		bins = np.insert(bins, [0, len(bins)], [-np.inf, np.inf])
		score_table = pd.crosstab(pd.cut(score, bins, include_lowest=True), target).reset_index()
		score_table.loc[:, 'Total']= score_table[[0,1]].sum(axis=1)
		score_table.loc['Total', :]= score_table.sum(axis=0)
		score_table['BADRATE'] = score_table[1] / (score_table[1]+score_table[0])
		score_table['DIST_BAD'] = score_table[1] / score_table[1].sum()
		score_table['DIST_GOOD'] = score_table[0] / score_table[0].sum()
		score_table['DIST'] = score_table['Total'] / score_table['Total'].loc['Total']
		bad_rate = target.sum() / target.count()
		target_above = target[score >= threshold_value]
		pop_above = score[score >= threshold_value].count() / score.count()
		pop_below = 1 - pop_above
		bad_rate_above = target_above.sum() / target_above.count()
		distribution_metrics = {'name': name,
								'badrate': bad_rate,
								'population_above_threshold': pop_above,
								'population_below_threshold': pop_below,
								'cumulative_bad_rate_above_threshold': bad_rate_above,
								'threshold': threshold_value}

		return score_table, distribution_metrics


	def fit(self, model, model_index, variables=None, model_type='nonlinear', data_source='woe', cv_scoring='roc_auc', scorecard=True, score=False, cut_off_score=None):
		"""
		Train the model with defined cross validation object.

		Parameter
		---------
		- model: estimator model object that contains .fit, .predict, and .predict_proba methods
		- model_index: model name to be use as a key
		- model_type: type of model {'linear', 'nonlinear'}
		- data_source: type of data to be used {'raw', 'woe'}
		- cv_scoring: 'str', cross validation scoring method
					  using sklearn.model_selection.cross_val_score
					  default: roc_auc

		"""
		if not (variables is None):
			self.variables = variables

		model_index = model_index.replace(" ", "")
		print("Model name:", model_index)
		
		self.model[model_index] = ModelData()
		self.model[model_index]['model_type']=model_type
		self.model[model_index]['data_source']=data_source
		self.model[model_index]['variables']=variables
		
		if model_type == 'linear':
			if data_source == 'woe':
				self.fit_linear(model, model_index=model_index, cv_scoring=cv_scoring)
			else:
				raise TypeError("data_source should be woe if you use linear model")
		elif model_type == 'nonlinear':
			if data_source == 'woe':
				self.fit_nonlinear(model, model_index=model_index, cv_scoring=cv_scoring, scorecard=scorecard, score=score, cut_off_score=cut_off_score)
			elif data_source == 'raw':
				self.fit_nonlinear_without_woe(model, model_index=model_index, cv_scoring=cv_scoring, cut_off_score=cut_off_score)
			else:
				raise TypeError("Invalid data_source {}".format(data_source))
		else:
			raise TypeError("Invalid model_type {}".format(model_type))
	
	def fit_linear(self, model, model_index, cv_scoring='roc_auc'):
		"""
		Train the model with defined cross validation object.
		Please call transform function first before using this method

		Generated attributes
			- model: fitted model object state
			- train['cv_scoring']: str, cross validation scoring type
			- train['cv_scores']: list, cross validation scores
			- train['woe_pivot']:  pandas.DataFrame, pivot table
			- train['trained_data']: pandas.DataFrame, train main data merged with data used for training (train.X)
									  there will be '_unused' suffix in this dataset to mark that the data were not used in training
			- test['trained_data']: pandas.DataFrame, out of time main data merged with data used for training (test.X)
								   there will be '_unused' suffix in this dataset to mark that the data were not used in training


		Parameter
		---------
		- model: estimator model object that contains .fit, .predict, and .predict_proba methods
		- cv_scoring: 'str', cross validation scoring method
					  using sklearn.model_selection.cross_val_score
					  default: roc_auc

		"""
		self.model[model_index]['model'] = clone(model)

		for col in self.variables + [self.flag]:
			if self.train.data_woe[col].isnull().any() == True:
				print("The data " + col + " contain missing values and missing values will be handled using missing handler defined at transformer creation.")

		# cross validation
		cv_scores = cross_val_score(estimator=self.model[model_index]['model'], X=self.train.data_woe[self.variables],
                                                 y=self.train.data_woe[self.flag], scoring=cv_scoring, cv=self.cv)
		cv_mean = cv_scores.mean()
		self.train['evaluation'] = {'CROSS VALIDATION': cv_mean}
		self.test['evaluation'] = {'CROSS VALIDATION': ''}
		print(f'Train AUC: {cv_scores}, mean: {cv_mean}')


		# model training
		self.model[model_index]['model'].fit(self.train.data_woe[self.variables], self.train.data_woe[self.flag])
		if self.model[model_index]['model'].coef_ is None:
			raise TypeError("Model has no coefficient variable, scorecard cannot be produced.")
		coefs = self.model[model_index]['model'].coef_.ravel()
		
		# scorecard
		self.pivot_tables = {}
		for i, col in enumerate(self.variables):
			print(col)
			train_pivot = self._generate_pivot_table(self.train, col)
			test_pivot = self._generate_pivot_table(self.test, col, score=True)

			pivot = train_pivot.join(test_pivot, how='left', lsuffix='_train', rsuffix='_test')
			pivot = pivot.reset_index()
			pivot["SCORE"] = round(coefs[i] * pivot[col+"_WOE"] * self.M * -1)
			self.pivot_tables[col] = pivot 
			if coefs[i] < 0:
				print("Warning: Negative coefficient for %s"%col)
			display(pivot)

		if self.model[model_index]['model'].intercept_ is None:
			raise TypeError("Model has no intercept, base score cannot be calculated.")
		b0 = self.model[model_index]['model'].intercept_[0]
		base_point = self.C - b0 * self.M
		print("Model base point: %3f"%base_point)
		self.base_point = base_point

	def fit_nonlinear(self, model, model_index, cv_scoring='roc_auc', scorecard=True, score=False, cut_off_score=None):
		"""
        Train a nonlinear model with defined cross validation object.
        Generated attributes
            - model: fitted model object state
            - train['cv_scoring']: str, cross validation scoring type
            - train['cv_scores']: list, cross validation scores
            - train['woe_pivot']:  pandas.DataFrame, pivot table
            - train['trained_data']: pandas.DataFrame, train main data merged with data used for training (train.X)
                                      there will be '_unused' suffix in this dataset to mark that the data were not used in training
            - test['trained_data']: pandas.DataFrame, out of time main data merged with data used for training (test.X)
                                   there will be '_unused' suffix in this dataset to mark that the data were not used in training
        Parameter
        ---------
        - model: nonlinear estimator model object that contains .fit, .predict, and .predict_proba methods
                The nonlinear models could be the sklearn Random Forest or Multi-layer Perceptron
        - cv_scoring: 'str', cross validation scoring method
                      using sklearn.model_selection.cross_val_score
                      default: roc_auc
        """
		self.model[model_index]['model'] = clone(model)

		for col in self.variables + [self.flag]:
			if self.train.data_woe[col].isnull().any() == True:
				print(
					"The data " + col + " contain missing values and missing values will be handled using missing handler defined at transformer creation.")

		# cross validation
		cv_scores = cross_val_score(estimator=self.model[model_index]['model'], X=self.train.data_woe[self.variables],
									y=self.train.data_woe[self.flag], n_jobs=-1, scoring=cv_scoring, cv=self.cv)
		cv_mean = cv_scores.mean()
		self.train['evaluation'] = {'CROSS VALIDATION': cv_mean}
		self.test['evaluation'] = {'CROSS VALIDATION': ''}
		print(f'Train AUC: {cv_scores}, mean: {cv_mean}')

		# model training
		self.model[model_index]['model'].fit(self.train.data_woe[self.variables], self.train.data_woe[self.flag])

		if scorecard:
			# scorecard
			self.pivot_tables = {}
			for i, col in enumerate(self.variables):
				print(col)
				train_pivot = self._generate_pivot_table(self.train, col)
				test_pivot = self._generate_pivot_table(self.test, col, score=score)

				pivot = train_pivot.join(test_pivot, how='left', lsuffix='_train', rsuffix='_test')
				pivot = pivot.reset_index()
				self.pivot_tables[col] = pivot
				display(pivot)

		self.evaluation_nonlinear(model_index=model_index, cut_off_score=cut_off_score)
	
	# Experimental Function to train nonlinear model without WOE
	def fit_nonlinear_without_woe(self, model, model_index, cv_scoring='roc_auc', cut_off_score=None):
		"""
        Train a nonlinear model with defined cross validation object.
        This model does not apply WOE transformation.
        Generated attributes
            - model: fitted model object state
            - train['cv_scoring']: str, cross validation scoring type
            - train['cv_scores']: list, cross validation scores
            - train['woe_pivot']:  pandas.DataFrame, pivot table
            - train['trained_data']: pandas.DataFrame, train main data merged with data used for training (train.X)
                                      there will be '_unused' suffix in this dataset to mark that the data were not used in training
            - test['trained_data']: pandas.DataFrame, out of time main data merged with data used for training (test.X)
                                   there will be '_unused' suffix in this dataset to mark that the data were not used in training
        Parameter
        ---------
        - model: nonlinear estimator model object that contains .fit, .predict, and .predict_proba methods
                The nonlinear models could be the sklearn Random Forest or Multi-layer Perceptron
        - cv_scoring: 'str', cross validation scoring method
                      using sklearn.model_selection.cross_val_score
                      default: roc_auc
        """
		self.model[model_index]['model'] = clone(model)

		self.label_encoder = {}

		for feature in self.variables:
			if (self.meta_data[feature]['data_type'] == "categorical"):
				self.label_encoder[feature] = preprocessing.LabelEncoder()
				self.label_encoder[feature].fit(
					np.concatenate((self.train.data[feature], self.test.data[feature])))
				self.train.data[feature] = self.label_encoder[feature].transform(self.train.data[feature])
				self.test.data[feature] = self.label_encoder[feature].transform(self.test.data[feature])

		# cross validation
		cv_scores = cross_val_score(estimator=self.model[model_index]['model'], X=self.train.data[self.variables],
									y=self.train.data[self.flag], n_jobs=-1, scoring=cv_scoring, cv=self.cv)
		cv_mean = cv_scores.mean()
		self.train['evaluation'] = {'CROSS VALIDATION': cv_mean}
		self.test['evaluation'] = {'CROSS VALIDATION': ''}
		print(f'Intime AUC: {cv_scores}, mean: {cv_mean}')

		# model training
		self.model[model_index]['model'].fit(self.train.data[self.variables], self.train.data[self.flag])
		
		self.evaluation_nonlinear_without_woe(model_index=model_index, cut_off_score=cut_off_score)
	
	def evaluation(self, model_index, cut_off_score=None):
		data_source = self.model[model_index]['data_source']
		model_type = self.model[model_index]['model_type']

		if model_type == 'linear':
			if data_source == 'woe':
				if cut_off_score is None:
					cut_off_score = 330

				self.evaluation_linear(model_index=model_index, cut_off_score=cut_off_score)
			else:
				raise TypeError("data_source should be woe if you use linear model")
		elif model_type == 'nonlinear':
			if data_source == 'woe':
				self.evaluation_nonlinear(model_index=model_index, cut_off_score=cut_off_score)
			elif data_source == 'raw':
				self.evaluation_nonlinear_without_woe(model_index=model_index, cut_off_score=cut_off_score)
			else:
				raise TypeError("Invalid data_source {}".format(data_source))
		else:
			raise TypeError("Invalid model_type {}".format(model_type))
	
	def evaluation_linear(self, model_index, cut_off_score=330):
		variables = self.model[model_index]['variables']

		# evaluation
		y_proba_train = self.model[model_index]['model'].predict_proba(self.train.data_woe[self.variables])[:, 1]
		y_proba_test = self.model[model_index]['model'].predict_proba(self.test.data_woe[self.variables])[:, 1]

		self.train['evaluation']['AUC'] = roc_auc_score(self.train.data_woe[self.flag], y_proba_train)
		self.test['evaluation']['AUC'] = roc_auc_score(self.test.data_woe[self.flag], y_proba_test)

		y_score_train = self.map_to_score(y_proba_train)
		y_score_test = self.map_to_score(y_proba_test)

		self.train['evaluation']['gte_'+str(cut_off_score)] = (y_score_train >= cut_off_score).astype(int).sum()
		self.train['evaluation']['lt_'+str(cut_off_score)] = (y_score_train < cut_off_score).astype(int).sum()

		self.test['evaluation']['gte_'+str(cut_off_score)] = (y_score_test >= cut_off_score).astype(int).sum()
		self.test['evaluation']['lt_'+str(cut_off_score)] = (y_score_test < cut_off_score).astype(int).sum()

		self.train['evaluation']['percentage_gte_'+str(cut_off_score)] = self.train['evaluation']['gte_'+str(cut_off_score)]/self.train.data.shape[0] 
		self.test['evaluation']['percentage_gte_'+str(cut_off_score)] = self.test['evaluation']['gte_'+str(cut_off_score)]/self.test.data.shape[0] 

		train_metrics = pd.DataFrame(self.train['evaluation'], index=['train'])
		test_metrics = pd.DataFrame(self.test['evaluation'], index=['test'])
		self.model[model_index]['evaluation'] = train_metrics.append(test_metrics)
		self.model[model_index]['evaluation'] = self.model[model_index]['evaluation'][['AUC', 'CROSS VALIDATION', 'gte_'+str(cut_off_score), 'lt_'+str(cut_off_score), 'percentage_gte_'+str(cut_off_score)]]
		
	def evaluation_nonlinear(self, model_index, cut_off_score=None):
		variables = self.model[model_index]['variables']

		# evaluation
		y_proba_train = self.model[model_index]['model'].predict_proba(self.train.data_woe[variables])[:, 1]
		y_proba_test = self.model[model_index]['model'].predict_proba(self.test.data_woe[variables])[:, 1]

		self.train['evaluation']['AUC'] = roc_auc_score(self.train.data_woe[self.flag], y_proba_train)
		self.test['evaluation']['AUC'] = roc_auc_score(self.test.data_woe[self.flag], y_proba_test)

		# Avoid division by zero exception (i.e. 1 - proba = 0)
		y_proba_train = np.where(y_proba_train == 1, 1 - 1e-10, y_proba_train)
		y_proba_test = np.where(y_proba_test == 1, 1 - 1e-10, y_proba_test)

		y_score_train = self.map_to_score(y_proba_train)
		y_score_test = self.map_to_score(y_proba_test)

		if not (cut_off_score is None):
			self.train['evaluation']['gte_'+str(cut_off_score)] = (y_score_train >= cut_off_score).astype(int).sum()
			self.train['evaluation']['lt_'+str(cut_off_score)] = (y_score_train < cut_off_score).astype(int).sum()

			self.test['evaluation']['gte_'+str(cut_off_score)] = (y_score_test >= cut_off_score).astype(int).sum()
			self.test['evaluation']['lt_'+str(cut_off_score)] = (y_score_test < cut_off_score).astype(int).sum()

			self.train['evaluation']['percentage_gte_'+str(cut_off_score)] = self.train['evaluation']['gte_'+str(cut_off_score)]/self.train.data_woe.shape[0]
			self.test['evaluation']['percentage_gte_'+str(cut_off_score)] = self.test['evaluation']['gte_'+str(cut_off_score)]/self.test.data_woe.shape[0]

		train_metrics = pd.DataFrame(self.train['evaluation'], index=['train'])
		test_metrics = pd.DataFrame(self.test['evaluation'], index=['test'])
		self.model[model_index]['evaluation'] = train_metrics.append(test_metrics)
		
		if not (cut_off_score is None):
			self.model[model_index]['evaluation'] = self.model[model_index]['evaluation'][
				['AUC', 'CROSS VALIDATION', 'gte_'+str(cut_off_score), 'lt_'+str(cut_off_score), 'percentage_gte_'+str(cut_off_score)]]
		else:
			self.model[model_index]['evaluation'] = self.model[model_index]['evaluation'][['AUC', 'CROSS VALIDATION']]
	
	def evaluation_nonlinear_without_woe(self, model_index, cut_off_score=None):
		variables = self.model[model_index]['variables']
		
		# evaluation
		y_proba_train = self.model[model_index]['model'].predict_proba(self.train.data[variables])[:, 1]
		y_proba_test = self.model[model_index]['model'].predict_proba(self.test.data[variables])[:, 1]

		self.train['evaluation']['AUC'] = roc_auc_score(self.train.data[self.flag], y_proba_train)
		self.test['evaluation']['AUC'] = roc_auc_score(self.test.data[self.flag], y_proba_test)

		# Avoid division by zero exception (i.e. 1 - proba = 0)
		y_proba_train = np.where(y_proba_train == 1, 1 - 1e-10, y_proba_train)
		y_proba_test = np.where(y_proba_test == 1, 1 - 1e-10, y_proba_test)

		y_score_train = self.map_to_score(y_proba_train)
		y_score_test = self.map_to_score(y_proba_test)

		if not (cut_off_score is None):
			self.train['evaluation']['gte_'+str(cut_off_score)] = (y_score_train >= cut_off_score).astype(int).sum()
			self.train['evaluation']['lt_'+str(cut_off_score)] = (y_score_train < cut_off_score).astype(int).sum()

			self.test['evaluation']['gte_'+str(cut_off_score)] = (y_score_test >= cut_off_score).astype(int).sum()
			self.test['evaluation']['lt_'+str(cut_off_score)] = (y_score_test < cut_off_score).astype(int).sum()

			self.train['evaluation']['percentage_gte_'+str(cut_off_score)] = self.train['evaluation']['gte_'+str(cut_off_score)]/self.train.data_woe.shape[0]
			self.test['evaluation']['percentage_gte_'+str(cut_off_score)] = self.test['evaluation']['gte_'+str(cut_off_score)]/self.test.data_woe.shape[0]

		train_metrics = pd.DataFrame(self.train['evaluation'], index=['train'])
		test_metrics = pd.DataFrame(self.test['evaluation'], index=['test'])
		self.model[model_index]['evaluation'] = train_metrics.append(test_metrics)
		
		if not (cut_off_score is None):
			self.model[model_index]['evaluation'] = self.model[model_index]['evaluation'][
				['AUC', 'CROSS VALIDATION', 'gte_'+str(cut_off_score), 'lt_'+str(cut_off_score), 'percentage_gte_'+str(cut_off_score)]]
		else:
			self.model[model_index]['evaluation'] = self.model[model_index]['evaluation'][['AUC', 'CROSS VALIDATION']]
	
	def predict_proba(self, data, model_index, verbose=True):
		"""
		Using the inputted data, woe transforms required columns and it to the model to output the predicted probabilities.

		Parameter
		---------
		- data: DataFrame, raw input data to be transformed as inputs to the model 
		- woe: Bool, No WOE transformation if sets to False
		- verbose: Bool 

		"""
		woe = (self.model[model_index]['data_source'] == 'woe')
		variables = self.model[model_index]['variables']

		if woe:
			data_bin = pd.DataFrame()
			data_woe = pd.DataFrame()
		
			for col in variables:
				if self.meta_data[col]['data_type'] == "categorical":
					data[col] = data[col].astype(str)
				if verbose:
					print("Binning and calculating woe for %s"%col)
				data_bin[col] = BinningBuilder.get_binning(data[col], self.meta_data[col])
				mapper = self.train['mapper'][col]
				data_woe[col] = self.bin_to_woe(data_bin[col], mapper, col)

			return self.model[model_index]['model'].predict_proba(data_woe[variables])
		else:
			for feature in variables:
				if (self.meta_data[feature]['data_type'] == "categorical"):
					self.label_encoder[feature] = preprocessing.LabelEncoder()
					self.label_encoder[feature].fit(np.concatenate(data[fature]))
					data[feature] = self.label_encoder[feature].transform(data[feature])

			return self.model[model_index]['model'].predict_proba(data[variables])

	def predict_score(self, data, model_index, verbose=True):
		"""
		Using inputted data, predicts the probabilities and convert the probability to score.

		Parameter
		---------
		- data: DataFrame, raw input data to be transformed as inputs to the model 
		- verbose: Bool 

		"""
		variables = self.model[model_index]['variables']
		y_proba = self.predict_proba(data[variables], model_index=model_index, verbose=verbose)[:, 1]

		# Avoid division by zero exception (i.e. 1 - proba = 0)
		y_proba = np.where(y_proba == 1, 1 - 1e-10, y_proba)

		probabilities = pd.Series(y_proba)
		score = self.map_to_score(probabilities)
		return score

	def map_to_score(self, probabilities):
		"""
		Transformation to convert probabilities to score

		Parameter
		---------
		- probabilities: Series, series of floats that represents probabilities (0-1)
		"""
		score = pd.Series(probabilities).apply(lambda probability: self.C - (self.M * np.log(probability / (1 - probability))))
		return score

	def score_to_proba(self, score):
		t = np.exp((self.C - score) / self.M)
		prob = t / (1 + t)
		return prob

	def train_data_metrics(self):
		tn, fp, fn, tp = self.train.confusion.ravel()
		self.compute_metric(tn, fp, fn, tp)

	def test_data_metrics(self):
		tn, fp, fn, tp = self.test.confusion.ravel()
		self.compute_metric(tn, fp, fn, tp)

	def compute_metric(self, tn, fp, fn, tp):
		accuracy = (tp + tn) / (tn + tp + fn + fp)
		precision = tp / (tp + fp)
		recall = tp / (tp + fn)
		f1 = 2 * (precision * recall) / (precision + recall)
		positive = tp + fn
		negative = tn + fp
		tpr = tp/(tp+fn)
		fpr = fp/(fp+tn)
		plr = tpr/fpr  # positive likelihood ratio
		print("{}: default={}, non-default={}, tp={}, tn={}, fp={}, fn={}, "
			  "TPR={:.3f}, FPR={:.3f}, LR+={:.3f}, f1={:.3f}".format(
			type(self.model).__name__, positive, negative, tp, tn, fp, fn,
			tpr, fpr, plr, f1))

	@staticmethod
	def map_to_score(probabilities):    
		"""
		Transformation to convert probabilities to score

		Parameter
		---------
		- probabilities: Series, series of floats that represents probabilities (0-1)
		"""
		M = Model.PDO / np.log(2.)
		C = Model.S0 + (M * np.log(1./Model.ODDS))  
		score = pd.Series(probabilities).apply(lambda probability: C - (M * np.log(probability / (1 - probability))))
		return score

	def set_binning(self, col, binning_data):
		self.meta_data[col]['binning_data'] = binning_data

	def generate_score_distribution(self, data, binning=20):
		"""
		Outputs a pivot table that contains the cumulative distribution of the score

		Parameter
		---------
		- data: DataFrame, raw input data to be transformed as inputs to the model 
		"""
		
		data['score'] = self.predict_score(data)	
		data['score_bin'] = pd.cut(data['score'], range(0, 1000, binning))
		pivot = pd.pivot_table(data, index=['score_bin'], values=self.key, columns=[self.flag], aggfunc='count').fillna(0)
		pivot['TOTAL'] = pivot[0] + pivot[1]
		pivot['BADRATE'] = pivot[1]/pivot['TOTAL']
		pivot["CUMSUM"] = pivot["TOTAL"].cumsum()/pivot["TOTAL"].sum()
		return pivot
			
	def _generate_pivot_table(self, dataset, col, score=False):
		merged_data = pd.merge(dataset.data_bin, dataset.data_woe[[self.key] + self.variables], on=self.key, suffixes=['', '_WOE'])
		pivot = pd.pivot_table(merged_data, index=[col, col+'_WOE'], values=self.key, columns=[self.flag], aggfunc='count')
		pivot["TOTAL"] = pivot[0] + pivot[1]
		pivot["BADRATE"] = pivot[1]/pivot["TOTAL"]

		if score:
			temp = merged_data[[col, col+'_WOE']].reset_index()
			temp["model_score"] = self.predict_score(self.test.data.reset_index(), verbose=False)
			pivot["median_score_test"] = temp.groupby([col,col+'_WOE'])['model_score'].median()
		
		return pivot

	def bin_to_woe(self, data_bin, mapper, col, verbose=False):
		default_bin = self.meta_data[col]['default_bin']
		if default_bin not in mapper:
			mapper[default_bin] = self.default_woe
		for binning in data_bin.unique():
			if binning not in mapper:
				mapper[binning] = self.default_woe
		data_woe = data_bin.replace(mapper)
		if verbose and data_woe.isna().any():
			print("Warning: Default WOE values used for col %s because some binnings are not available in mapping"%col)
		data_woe = data_woe.replace(np.nan, self.default_woe)
		if verbose and ((data_woe == np.inf) | (data_woe == -np.inf)).any():
			print("Warning: Default WOE values used for col %s because some binnings values are inf"%col)
		data_woe = data_woe.replace(np.inf, self.default_woe)
		data_woe = data_woe.replace(-np.inf, self.default_woe)
		return data_woe

	def save_meta_data_to_file(self, path):
		"""
		Save meta data (required information to describe each feature) to a csv file.

		Parameter
		---------
		- path: 'str', Destination for meta data file 
		"""
		to_save = self.meta_data.copy()
		for key in to_save:
			binning_data = to_save[key]['binning_data']
			to_save[key]['binning_data'] = json.dumps(binning_data)
		meta_data = pd.DataFrame(to_save)
		meta_data.to_csv(path)

	def save_meta_data_to_dict(self):
		"""
		Outputs meta data (required information to describe each feature) to a dictionary.
		"""
		return self.meta_data

	def save_to_excel(self, model_index, path, date_columns=["approvedDate"]):
		"""
		Saves train data, test data, scorecard and performance to excel.

		Parameter
		---------
		path (str): Path to the .xlsx file to save the data.
		date_columns (list): Columns to convert to excel date format %m-%d-%Y

		"""
		train_data = self.train.data.copy()
		test_data = self.test.data.copy()
		evaluation = self.model[model_index]['evaluation'].copy()

		for date in date_columns:
			train_data[date] = self.train.data[date].dt.strftime("%m-%d-%Y")
			test_data[date] = self.test.data[date].dt.strftime("%m-%d-%Y")

		train_data["product_type"] = self.train.data[self.key].str.slice(0,3)
		test_data["product_type"] = self.test.data[self.key].str.slice(0,3)

		scorecard = pd.DataFrame()

		for var in self.variables:
			temp_pivot = self.pivot_tables[var].copy()
			temp_pivot['var_name'] = var
			temp_pivot = temp_pivot.rename(columns={var: 'bins'})
			scorecard = scorecard.append(temp_pivot, ignore_index=True)
		
		scorecard = scorecard.append(pd.DataFrame({'var_name': ['base_score'], 'SCORE': self.base_point}), ignore_index=True)
		scorecard = scorecard[['var_name', 'bins','0_train', '1_train',
							 'BADRATE_train', #'DIST_BAD', 'DIST_GOOD', 'WOE', 'weights',
							 '0_test', '1_test', 'BADRATE_test', 'SCORE']]

		writer = pd.ExcelWriter(path,
			options={'remove_timezone': True},
			engine='xlsxwriter')
		
		train_data.to_excel(writer, sheet_name='train_data')
		test_data.to_excel(writer, sheet_name='test_data')
		scorecard.to_excel(writer, sheet_name='scorecard')
		evaluation.to_excel(writer, sheet_name='model_performance')

		writer.save()
		print(f"Excel saved to {path}")

	@staticmethod
	def _replace_inf(dic):
		for k,v in dic.items():
			if v == np.inf:
				dic[k] = "inf"
			if v == -np.inf:
				dic[k] = "-inf"
		return dic

	def generate_json(self, path, feature_rule_mapping, verbose=False):
		"""
		Generates JSON file for model definition.

		Parameters
		---------
		path (str): Path to the .json or .pkl file to save the model.
		feature_rule_mapping (dictionary): dictionary mapping of features to its rule 
		"""
		json_dict = {}
		json_dict["rules"] = {}

		for feature_name in self.variables:
			if feature_name in feature_rule_mapping:
				rule_name = feature_rule_mapping[feature_name]

				if rule_name not in json_dict["rules"]:
					json_dict["rules"][rule_name] = {}
				meta_data = self.meta_data[feature_name]
				temp_pivot = self.pivot_tables[feature_name]
				score_list = []
				if meta_data['data_type'] == 'categorical':
					default_assigned = False
					reverse_dic = {}
					for key in meta_data['binning_data']:
						val = meta_data['binning_data'][key]
						if val not in reverse_dic:
							reverse_dic[val] = []
						reverse_dic[val].append(key)
					for key in reverse_dic:
						temp_dic = {}
						row = temp_pivot[temp_pivot[feature_name] == key]
						if row.shape[0] > 0:
							temp_dic['score'] = row['SCORE'].values[0]
							temp_dic['in'] = reverse_dic[key]
							if meta_data['default_bin'] in reverse_dic[key]:
								temp_dic['default'] = True
								default_assigned = True
							score_list.append(temp_dic)
					if not default_assigned:
						score_list.append({'score': self.default_woe, 'in': [meta_data['default_bin']], 'default': True})
				else:
					for i in range(temp_pivot.shape[0]):
						temp_dict = {}
						interval = temp_pivot[feature_name].values[i]
						temp_dict['min'] = interval.left
						temp_dict['max'] = interval.right
						temp_dict['score'] = temp_pivot['SCORE'].values[i]
						score_list.append(temp_dict)
					score_list = [Model._replace_inf(dic) for dic in score_list]
				json_dict["rules"][rule_name][feature_name] = score_list 
			else:
				print("Warning! %s not in feature rule mapping."%feature_name)

		if verbose:
			display(json_dict)

		if ".pkl" in path:
			with open(path, 'wb') as file:
				pickle.dump(json_dict, file, protocol=pickle.HIGHEST_PROTOCOL)
		else:
			with open(path, "w") as file:
				json.dump(json_dict, file)
